#include "lib/util.h"
#include "core.h"
#include "main_class.h"

int main(int argc) {
	cout << "Loading..." << endl;
	bool testing = false;
	if (argc >= 2) testing = true;

	//Spawn main, Look for Existing data, and try to load it
	Main *app = new Main(testing);

	//Initialize Data Structures
	app->init();

	//Run the AI
	int status = app->run();
	return status;
}