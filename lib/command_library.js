'use strict';

const path = require('path');
const fs = require('fs');
const chalk = require('chalk');
global.dir = path.resolve(__dirname, "battles/");

class Operation {
	constructor(id, method) {
		this.id = id;
		this.method = method;
	}

	async run(ctx, args) {
		return new Promise(async (resolve, reject) => {
			const results = await this.method.call(ctx, ...args);
			resolve(results);
		});
	}
};

module.exports = {
	fetch: function(id) {
		id = id.trim();
		if (Op[id]) return new Operation(id, Op[id]);
		return null;
	},

	noOp: function(cmd) {
		console.log("--Invalid Command: " + cmd + " - type `help` for a list of commands.");
		prompt();
	}
};

//Operations

const Op = {
	help: function() {
		const out = "- `start`: Starts SubProcess.\n" +
					"- `reload`: Reloads SubProcess.\n" +
					"- `setloc` [path/to/dir]: Sets the location where battle log files are loaded from. Default is `battles/`\n" +
					"- `send` [-options]: Begins sending logs to be parsed. Supports the following Arguments - \n" +
					"--- `-n=[int]`: Sets the amount of logs to parse. Defaults to all files in Directory.\n" + 
					"--- `-v`: Enables Verbose Logging.\n" +
					"- `exit`: Closes Application.\n";
		console.log(out);
		return false;
	},

	start: function() {
		init();
		return false;
	},

	reload: function() {
		if (!Main || !Main.process || !Main.process.send) {
			init();
			return false;
		}
		return new Promise((resolve, reject) => {
			Main.process.once("close", (code) => {
				init();
				resolve(false);
			});
			Main.send("exit");
		});
	},

	setloc: function(dir) {
		if (!dir) {
			console.log("You must specify a directory.");
			return false;
		}
		const parts = dir.split(path.sep);
		const p = path.resolve(...parts);
		dir = p;

		return false;
	},

	send: function(...options) {
		return new Promise((resolve, reject) => {
			let num = null;
			let verbose = false;
			let err = false;

			if (options) {
				for (let i = 0; i < options.length; i++) {
					switch(true) {
					case options[i].includes("-n"):
						if (!options[i].includes("=")) {
							err = "Err: No Value set. Example - `send -n=3`";
							break;
						}
						options[i].replace("-n","").replace("=","").replace(" ","");
						if (isNaN(parseInt(options[i]))) break;
						num = parseInt(options[i]);
						break;
					case options[i].includes("v"): verbose = true; break;
					}
				}
			}

			if (err) {
				console.log(chalk.red(err));
				return false; //Not Fatal
			}

			let filenames = null;
			try {
				filenames = fs.readdirSync(dir);
			} catch (e) {
				err = e;
			}

			if (err || !filenames || filenames.length === 0) {
				console.log(chalk.red(err ? err : "Err: No Files Could be located"));
				return false;
			}

			if (num && !isNaN(num) && num < filenames.length) {
				let newfiles = []
				for (let i = 0; i < num; i++) {
					newfiles.push(filenames[i]);
				}
				filenames = newfiles;
			}

			const files = [];

			let buffer;
			for (const file of filenames) {
				try {
					buffer = fs.readFileSync(path.resolve(dir, file), "utf-8");
				} catch (e) {
					err = e;
				}
				if (err || !buffer) {
					if (!err) err = "Err: No File Data in " + file;
					break;
				} else {
					files.push(buffer);
					buffer = null;
				}
			}

			if (err || files.length === 0) {
				console.log(chalk.red(err ? err : "Err: No Files Could be loaded"));
				return false;
			}

			num = files.length;
			let counter = 0;
			let timeoutperiod = num * 1000 * 30; //Timeout Scales based on the amount of files being parsed
			let timeout = setTimeout(() => {
				console.log(chalk.red.bold("Timeout Reached - Exiting..."));
				return resolve(true);
			}, timeoutperiod);

			Main.on("parsed", () => {
				counter++;
				if (counter === num) {
					clearTimeout(timeout);
					return resolve(false);
				}
			});

			for (const file of files) {
				Main.send(file);
			}
		});
	},

	exit: function () {
		if (!Main || !Main.process || !Main.process.send) {
			console.log("Exiting...");
			process.exit();
		}
		return new Promise((resolve, reject) => {
			Main.process.once("close", (code) => {
				console.log("Exiting...");
				process.exit();
			});
			Main.send("exit");
		});
	}
};