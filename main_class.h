
#ifndef MAIN_CLASS_H
#define MAIN_CLASS_H

#include "modules.h"

namespace JSON {
	Battle parse(const string JSON) {
		Battle b(JSON);
		return b;
	}
}

class Main : public Module {
public:
	bool verbose;
	Main(bool verbose) {
		if (verbose) {
			this->verbose = true;
			cout << "Verbose Mode Enabled." << endl;
		} else {
			this->verbose = false;
		}
	}

	int init() {
		string data = this->loader->getData();

		if (data != "nodata") {
			//Initialize previous data
		} else {
			this->firstRun(data);
		}

		this->status = ready;
	}

	int run() {
	string buffer;
	c1: //Label to reset Loop
		cout << "ready" << endl;
		while(this->status == ready) {
			while(getline(cin,buffer)) this->status = this->receive(buffer);
		}

	c2: //Label to reset Status Parsing

		if (this->status == error) {
			//Catch Error and do something
			if (this->status != error) goto c2;
		} else {
			switch(this->status) {
			case success:
				this->status = ready;
				goto c1;
				break;
			case complete: return 0;
			}
		}
	}
private:
	Loader *loader = new Loader;
	Mango *mango = new Mango;
	map<string, Pokemon*> pokes;

	void firstRun(const string data) {
		for (unsigned int i = 0; i < sizeof(pokelist) / sizeof(pokelist[0]); i++) {
			Pokemon *poke = new Pokemon(pokelist[i]);
			this->pokes[pokelist[i]] = poke;
		}
	}

	Status receive(string data) {
		if (data == "exit") {
			cout << "Received Exit From Wrapper, Exiting SubProcess..." << endl;
			return complete;
		} else if (data == "getawait") {
			ostringstream ss;
			vector<string> list = Brain->getAwaiting();
			for (unsigned int i = 0; i < sizeof(list) / sizeof(list[0]); i++) {
				ss << list[i] << ",";
			}
			cout << "awaitlist," << ss.str() << "\n";
			return success;
		} else {
			return this->parse(data);
		}
	}

	Status parse(string data) {
		Battle battle = JSON::parse(data);

		Status results = this->mango->parse(this->pokes, battle);
		if (results != error) {
			cout << "battleparsed" << endl;
			this->calcTiers();
		} else {
			cerr << "Error Parsing Battle Data." << endl;
		}
		return results;
	}

	void calcTiers() {
		map<string, float> ladder;
		vector<float> vals;
		map<string, Pokemon*>::iterator it;
		int total = 0;
		for (it = this->pokes.begin(); it != this->pokes.end(); it++) {
			ladder[it->first] = it->second->e();
			vals.push_back(it->second->e());
			total++;
		}

		map<string, float>::iterator x;
		map<string, float> pcts;
		for (x = ladder.begin(); x != ladder.end(); x++) {
			pcts[x->first] = pct(vals, x->second);
		}

		map<string, Tier> list;
		for (x = pcts.begin(); x != pcts.end(); x++) {
			Tier t;
			if (x->second > 90) {
				t = A;
			} else if (x->second > 80) {
				t = B;
			} else if (x->second > 70) {
				t = C;
			} else if (x->second > 60) {
				t = D;
			} else if (x->second > 50) {
				t = E;
			} else if (x->second > 40) {
				t = F;
			} else if (x->second > 30) {
				t = G;
			} else if (x->second > 20) {
				t = H;
			} else if (x->second > 10) {
				t = I;
			} else if (x->second <= 10) {
				t = J;
			} else {
				t = Z;
			}
		}

		vector<string> stier = Brain->getStier();

		if (stier.size() > 0) {
			for (unsigned int z = 0; z < sizeof(stier) / sizeof(stier[0]); z++) {
				list[stier[z]] = S;
			}
		}

		//Export Tiers

		ostringstream ss;

		auto end = chrono::system_clock::now();
		time_t end_time = chrono::system_clock::to_time_t(end);
		map<string, Tier>::iterator y;

		ss << "Spectra-Meta Tierlist - Generated(" << std::put_time(localtime(&end_time), "%Y-%m-%d %X") << ")\n\n";

		Tier tier[12] = {S, A, B, C, D, E, F, G, H, I, J, Z};
		string title[12] = {"S Tier (banned)", "A Tier", "B Tier", "C Tier", "D Tier", "E Tier", "F Tier", "G Tier", "H Tier", "I Tier", "J Tier", "Z Tier (Unrated)"};

		for (unsigned int i = 0; i < 12; i++) {
			ss << title[i] << " {\n";

			for (y = list.begin(); y != list.end(); y++) {
				if (y->second == tier[i]) {
					ss << y->first << " (" << pcts[y->first] << ")\n";
				}
			}

			ss << "}\n\n";
		}

		ss << "[This File was Automatically Generated. Do Not Edit.]";

		ostringstream filename;
		filename << "exports/tierlist_" << std::put_time(localtime(&end_time), "%Y-%m-%d %X") << ".log";
		ofstream stream(filename.str());

		if (stream.is_open()) {
			stream << ss.str();
			stream.close();
		}

		cout << "tiersregen" << endl;
	}
};

#endif