
#ifndef CORE_H
#define CORE_H

class brain; //Forward Declare
class SMon;

struct K {
	string c;
	float v;

	K(string category, float value) {
		this->c = category;
		this->v = value;
	}

	void modify(float d) {
		this->v += d;
	}

	void apply(SMon *m, float f);
};

struct M {
	string id;
	map<string, K> k;
	M(string id, map<string, K> k) {
		this->id = id;
		this->k = k;
	}

	void mod(string id, float d) {
		map<string, K>::iterator i = this->k.find(id);
		if (i != this->k.end()) {
			i->second.modify(d);
		}
	}

	string stringify() {
		string out = this->id + "|";
		map<string, K>::iterator it;
		for (it = this->k.begin(); it != this->k.end(); it++) {
			out += it->first + "=" + to_string(it->second.v) + ",";
		}

		out.pop_back();
		return out;
	}
};

class Rating;

struct Br {
	map<string, M> m; //Brain Data
	map<string, float> w; //Weights
	vector<string> a; //Awaiting
	vector<string> s; //S tier

	Br() {
		this->m = this->loadBrain();
		this->w = this->loadWeights();
		this->a = this->loadAwaiting();
		this->s = this->loadStier();
	}

	void save() {
		this->writeBrain();
		this->writeWeights();
		this->writeAwaiting();
		this->writeStier();
	}

	map<string, float> getWeights() {
		return this->w;
	}

	vector<string> getAwaiting() {
		return this->a;
	}

	vector<string> getStier() {
		return this->s;
	}

	float calcEff(vector<map<string, Rating>> protos);
	void receive(SMon *mon, const string id, const float d);
	map<string, float> applyWeights(map<string, Rating> proto);

	float combine(map<string, float> ratings) {
		float out = 0;
		float div = 9;
		map<string, float>::iterator it;
		for (it = ratings.begin(); it != ratings.end(); it++) {
			out += it->second;
		}
		out = static_cast<float> (out / div);
		return out;
	}

	void noHandler(const string id) {
		bool exists = false;
		for (unsigned int i = 0; sizeof(this->a) / sizeof(this->a[0]); i++) {
			if (this->a[i] == id) exists = true;
		}

		if (exists) return; //Already awaiting

		this->a.push_back(id);
		this->writeAwaiting();
		cout << "awaiting," << id << endl;
	}

	map<string, M> loadBrain() {
		ifstream stream("data/brain.csv");
		string buffer;
		map<string, M> br;

		if (stream.is_open()) {
			while(getline(stream,buffer)) this->unpack(buffer, br);
			stream.close();
		}

		return br;
	}

	map<string, float> loadWeights() {
		ifstream stream("data/weights.csv");
		string buffer;
		map<string, float> out;

		if (stream.is_open()) {
			while(getline(stream, buffer)) {
				string::size_type ptr0 = buffer.find("=");
				out[buffer.substr(0, ptr0)] = stof(buffer.substr(ptr0 + sizeof(buffer.at(0)), buffer.find("\n")));
			}
			stream.close();
		}

		return out;
	}

	vector<string> loadAwaiting() {
		ifstream stream("data/awaiting.csv");
		string buffer;
		vector<string> out;

		if (stream.is_open()) {
			while(getline(stream, buffer)) out.push_back(buffer);
			stream.close();
		}

		return out;
	}

	vector<string> loadStier() {
		ifstream stream("data/stier.csv");
		string buffer;
		vector<string> out;

		if (stream.is_open()) {
			while(getline(stream, buffer)) out.push_back(buffer);
			stream.close();
		}

		return out;
	}

	void writeBrain() {
		ostringstream data = this->pack();
		ofstream stream("data/brain.csv");
		if (stream.is_open()) {
			stream << data.str();
			stream.close();
		}
	}

	void writeWeights() {
		ofstream stream("data/weights.csv");
		if (stream.is_open()) {
			map<string, float>::iterator it;
			for (it = this->w.begin(); it != this->w.end(); it++) {
				stream << it->first << "=" << it->second << "\n";
			}
			stream.close();
		}
	}

	void writeAwaiting() {
		ofstream stream("data/awaiting.csv");
		if (stream.is_open()) {
			for (unsigned int i = 0; sizeof(this->a) / sizeof(this->a[0]); i++) {
				stream << this->a[i] << "\n";
			}
			stream.close();
		}
	}

	void writeStier() {
		ofstream stream("data/stier.csv");
		if (stream.is_open()) {
			for (unsigned int i = 0; sizeof(this->a) / sizeof(this->a[0]); i++) {
				stream << this->a[i] << "\n";
			}
			stream.close();
		}
	}

	bool has(const string id) {
		map<string, M>::iterator i = this->m.find(id);
		if (i != this->m.end()) {
			return true;
		} else {
			return false;
		}
	}

	M get(const string id) {
		map<string, M>::iterator i = this->m.find(id);
		if (i != this->m.end()) {
			return i->second;
		}
	}

	void unpack(string buffer, map<string, M> &br) {
		const string d = "|";

		string id = "null";
		string::size_type ptr = buffer.find(d);

		if (ptr != string::npos) {
			id = buffer.substr(0, ptr);
			buffer.erase(0, ptr + sizeof(buffer.at(0)));
		}

		if (id != "null") {
			vector<string> Ks = split(buffer, ",");
			map<string, K> built;

			for (unsigned int i = 0; i < sizeof(Ks) / sizeof(Ks[0]); i++) {
				vector<string> parts = split(Ks[i], "=");
				string key = parts[0];
				float value = stof(parts[1]);
				K tk(key, value);
				built.insert(pair<string, K>(key, tk));
			}

			M m(id, built);

			br.insert(pair<string, M>(id, m));
		}
	}

	ostringstream pack() {
		ostringstream ss;

		map<string, M>::iterator it;
		for (it = this->m.begin(); it != this->m.end(); it++) {
			ss << it->second.stringify() << "\n";
		}

		return ss;
	}
} *Brain;



//Tier groupings for sorting
//S = Banned
//A = top 10%
//B = top 20%
//C = top 30%
//D = top 40%
//E = top 50%
//F = top 60%
//G = top 70%
//H = top 80%
//I = top 90%
//J = top 100%
//Z = Un-rated
enum Tier {S, A, B, C, D, E, F, G, H, I, J, Z};
enum Status {uninitialized, initialized, waiting, ready, complete, error, success, failed};

//Rating Categories
const string rc[9] = {"sweep","sweepbreak","stall","stallbreak","support","momentum","offense","defense","special"};

//Category Parameters

//Sweep - Efficacy at knocking out consecutive pokemon and cleaning up
//Sweepbreak - Efficacy at stopping enemy sweepers
//Stall - Efficacy at staying alive and slowly wearing enemy pokemon down
//Stallbreak - Efficacy at stopping enemy Stallers
//Support - Efficacy at playing a support role in maintaining and removing hazards, screens, and healing
//Momentum - Efficacy at keeping Switch initiative in favor and forcing the enemy to play reactively
//Offense - Efficacy at generally maintaining Offensive pressure
//Defense - Efficacy at generally minimizing enemy Offensive pressure
//Special - Efficacy at things that the other parameters cannot properly quantify

struct RDEF {
	const float MAX = 2000;
	const float DEF = 1000;
	const float MIN = 0;
};

struct PDEF {
	const float MIN_FREQ = .18;
	const float REM_FREQ = .095;
};

struct POKEDEF {
	const float EFF = 1000;
};

struct DEF {
	RDEF Ratings;
	PDEF Prototypes;
	POKEDEF Pokes;
} DEFAULTS;

class Module {
public:
	Status status = uninitialized;
	Module(){}
};

class Rating {
public:
	float val;
	Rating(float val = DEFAULTS.Ratings.DEF) {
		this->val = val;
	}

	float getValue() {
		return this->val;
	}

	void operator+=(const float value) {
		int oldVal = this->val;
		this->pushHistory(oldVal);
		this->val += value;
	}

	float operator[](const int index) {
		return this->history[index];
	}

	deque<int> getHistory(const int index) {
		return this->history;
	}
private:
	deque<int> history;

	void pushHistory(float val) {
		this->history.push_front(val);
		while(this->history.size() > 100) this->history.pop_back();
	}
};

class Prototype {
public:
	string id;
	Prototype(const string id) {
		this->id = id;
		this->crx = id;

		for (unsigned int i = 0; i < sizeof(rc) / sizeof(rc[0]); i++) {
			Rating r;
			this->ratings[rc[i]] = r;
		}
	}

	void update(map<string, float> changes) {
		map<string, float>::iterator it;
		for (it = changes.begin(); it != changes.end(); it++) {
			this->modify(it->first, it->second);
		}
	}

	string getCrx() {
		return this->crx;
	}

	map<string, Rating> getRatings() {
		return this->ratings;
	}
private:
	map<string, Rating> ratings;
	string crx;

	void modify(const string r, const float v) {
		map<string, Rating>::iterator p = this->ratings.find(r);
		if (p != this->ratings.end()) {
			this->ratings[r] += v;
		}
	}
};

//These are needed for the Pokemon class

map<string, float> Br::applyWeights(map<string, Rating> proto) {
	map<string, float> out;
	map<string, Rating>::iterator it;
	for (it = proto.begin(); it != proto.end(); it++) {
		out[it->first] = it->second.getValue() * this->w[it->first];
	}
	return out;
}

float Br::calcEff(vector<map<string, Rating>> protos) {
	vector<float> eff;
	unsigned int i;
	for (i = 0; i < sizeof(protos) / sizeof(protos[0]); i++) {
		map<string, float> weighted = this->applyWeights(protos[i]);
		eff.push_back(this->combine(weighted));
	}

	float final = 0;

	for (i = 0; i < sizeof(eff) / sizeof(eff[0]); i++) {
		final += eff[i];
	}

	final = static_cast<float> (final / i);
	return final;
}

class Pokemon {
public:
	string id;
	Pokemon(const string id, float eff = DEFAULTS.Pokes.EFF) {
		this->id = id;
		this->efficacy = eff;
		this->newProto("all");
	}

	Prototype &getProto(string id) {
		map<string, Prototype>::iterator it;
		for (it = this->prototypes.begin(); it != this->prototypes.end(); it++) {
			if (it->first == id) return it->second;
		}
	}

	void update() {
		map<string, Prototype>::iterator p;
		vector<map<string, Rating>> protoRatings;
		for (p = this->prototypes.begin(); p != this->prototypes.end(); p++) {
			protoRatings.push_back(p->second.getRatings());
		}

		float efficacy = Brain->calcEff(protoRatings);
		this->efficacy = efficacy;
	}

	int e() {
		return this->efficacy;
	}

	vector<Prototype> instance(vector<string> moves, string item, string abil, map<string, int> evs, map<string, int> ivs) {
		//This is called anytime a pokemon appears in a battle we're parsing
		this->pushHistory(moves,item,abil,evs,ivs);
		this->updateProtos();

		vector<Prototype> protos;
		protos.push_back(this->getProto("all"));

		vector<string> identifiers;
		for (unsigned int i = 0; i < sizeof(moves) / sizeof(moves[0]); i++) {
			identifiers.push_back("move|" + moves[i]);
		}
		identifiers.push_back("item|" + item);
		identifiers.push_back("abil|" + abil);

		for (unsigned int i = 0; i < sizeof(identifiers) / sizeof(identifiers[0]); i++) {
			if (this->hasProto(identifiers[i])) protos.push_back(identifiers[i]);
		}

		return protos;
	}
private:
	map<string, Prototype> prototypes;
	float efficacy;

	//History
	deque<vector<string>> hmoves;
	deque<string> hitems;
	deque<string> habil;
	deque<map<string, int>> hevs;
	deque<map<string, int>> hivs;

	void pushHistory(vector<string> moves, string item, string abil, map<string, int> evs, map<string, int> ivs) {
		this->hmoves.push_front(moves);
		while(this->hmoves.size() > 100) this->hmoves.pop_back();
		this->hitems.push_front(item);
		while(this->hitems.size() > 100) this->hitems.pop_back();
		this->habil.push_front(abil);
		while(this->habil.size() > 100) this->habil.pop_back();
		this->hevs.push_front(evs);
		while(this->hevs.size() > 100) this->hevs.pop_back();
		this->hivs.push_front(ivs);
		while(this->hivs.size() > 100) this->hivs.pop_back();
	}

	void updateProtos() {
		map<string, int> freq;
		int iterations = 0;

		if (this->hmoves.size() > 0) {
			for (unsigned int i = 0; i < sizeof(this->hmoves) / sizeof(this->hmoves[0]); i++) {
				for (unsigned int j = 0; j < sizeof(this->hmoves[i]) / sizeof(this->hmoves[i][0]); j++) {
					if (freq.find("move|" + this->hmoves[i][j]) != freq.end()) {
						freq["move|" + this->hmoves[i][j]]++;
					} else {
						freq["move|" + this->hmoves[i][j]] = 1;
					}
					iterations++;
				}
			}
		}

		if (this->hitems.size() > 0) {
			for (unsigned int i = 0; i < sizeof(this->hitems) / sizeof(this->hitems[0]); i++) {
				if (freq.find("item|" + this->hitems[i]) != freq.end()) {
					freq["item|" + this->hitems[i]]++;
				} else {
					freq["item|" + this->hitems[i]] = 1;
				}
				iterations++;
			}
		}

		if (this->habil.size() > 0) {
			for (unsigned int i = 0; i < sizeof(this->habil) / sizeof(this->habil[0]); i++) {
				if (freq.find("item|" + this->habil[i]) != freq.end()) {
					freq["item|" + this->habil[i]]++;
				} else {
					freq["item|" + this->habil[i]] = 1;
				}
				iterations++;
			}
		}

		if (iterations < 240) return; //Not enough instances to generate protos

		map<string, int> top;
		map<string, int> existing;
		for (unsigned int i = 0; i < 3; i++) {
			map<string, int>::iterator it;
			string h = "null";
			int hi = 0;
			for (it = freq.begin(); it != freq.end(); it++) {
				if (this->hasProto(it->first)) {
					existing[it->first] = it->second;
					continue;
				}
				if (top.find(it->first) != top.end()) continue;
				if (h == "null") {
					h = it->first;
					hi = it->second;
				} else {
					if (hi < it->second) {
						h = it->first;
						hi = it->second;
					}
				}
			}
			top[h] = hi;
		}

		map<string, int>::iterator it;
		for (it = existing.begin(); it != existing.end(); it++) {
			float removepercent = static_cast<float>(it->second) / static_cast<float>(iterations);
			if (removepercent > DEFAULTS.Prototypes.REM_FREQ) this->removeProto(it->first);
		}

		bool esc = false;
		while(this->prototypes.size() < 5 && !esc) {
			vector<string> valid;

			for (it = top.begin(); it != top.end(); it++) {
				float addpercent = static_cast<float>(it->second) / static_cast<float>(iterations);
				if (addpercent > DEFAULTS.Prototypes.MIN_FREQ) valid.push_back(it->first);
			}

			if (valid.size() == 0) return; //No valid prototypes

			for (unsigned int i = 0; i < sizeof(valid) / sizeof(valid[0]); i++) {
				if (!this->hasProto(valid[i])) {
					if (this->prototypes.size() < 5) {
						this->newProto(valid[i]);
					} else {
						esc = true;
						break;
					}
				}
			}
		}
	}

	void newProto(const string id) {
		if (this->hasProto(id)) return;
		Prototype proto(id);
		this->prototypes.insert(pair<string, Prototype>(id, proto));
	}

	void removeProto(const string id) {
		if (!this->hasProto(id)) return;
		this->prototypes.erase(id);
	}

	bool hasProto(const string id) {
		map<string, Prototype>::iterator it;
		for (it = this->prototypes.begin(); it != this->prototypes.end(); it++) {
			if (it->first == id) return true;
		}
		return false;
	}
};

class Mon {
public:
	Mon(){}

	void setName(string name) {
		this->name = name;
	}

	void setSpecies(string species) {
		this->species = toId(species);
	}

	void setMoves(vector<string> moves) {
		this->moves = moves;
	}

	void setAbility(string ability) {
		this->ability = toId(ability);
	}

	void setEvs(map<string, int> evs) {
		this->evs = evs;
	}

	void setIvs(map<string, int> ivs) {
		this->ivs = ivs;
	}

	void setItem(string item = "null") {
		this->item = toId(item);
	}

	void setLevel(int level = 100) {
		this->level = level;
	}

	void setShiny(bool shiny = false) {
		this->shiny = shiny;
	}

	string getName() {
		return this->name;
	}

	string getSpecies() {
		return this->species;
	}

	vector<string> getMoves() {
		return this->moves;
	}

	string getAbility() {
		return this->ability;
	}

	map<string, int> getEvs() {
		return this->evs;
	}

	map<string, int> getIvs() {
		return this->ivs;
	}

	string getItem() {
		return this->item;
	}

	int getLevel() {
		return this->level;
	}

	bool getShiny() {
		return this->shiny;
	}
private:
	string name; //Arbitrary
	string species; //ToID
	vector<string> moves;
	string ability; //ToID
	map<string, int> evs;
	map<string, int> ivs;
	string item; //ToID
	int level;
	bool shiny; //Arbitrary
};

class Team {
public:
	Team(){} //Unfilled
	Team(vector<Mon> mons) {
		this->list = mons;
	}

	vector<Mon> getTeam() {
		return this->list;
	}
private:
	vector<Mon> list;
};

class Event {
public:
	Event(string type, map<string, string> args) {
		this->type = type;
		this->args = args;
	}

	map<string, string> getArgs() {
		return this->args;
	}

	string getType() {
		return this->type;
	}
private:
	string type;
	map<string, string> args;
};

class Turn {
public:
	Turn(int num, string p1 = "null", string p2 = "null") {
		this->num = num;
		this->p1s.push_back(p1);
		this->p2s.push_back(p2);
	}

	void switchin(const string player, const string id) {
		string from;
		string enemy;
		if (player == "p1") {
			from = this->p1s.back();
			enemy = this->p2s.front();
			this->p1s.push_back(id);
		} else {
			from = this->p2s.back();
			enemy = this->p1s.front();
			this->p2s.push_back(id);
		}
		map<string, string> args;
		args["player"] = player;
		args["from"] = from;
		args["id"] = id;
		args["enemy"] = enemy;
		Event ev("switch", args);
		this->events.push_back(ev);
	}

	void move(const string player, const string poke, const string move) {
		string target;
		if (player == "p1") {
			target = this->p2s.back();
		} else {
			target = this->p1s.back();
		}
		map<string, string> args;
		args["player"] = player;
		args["poke"] = poke;
		args["target"] = target;
		args["move"] = move;
		Event ev("move", args);
		this->events.push_back(ev);
	}

	void mega(const string player, const string poke, const string item) {
		string enemy;
		if (player == "p1") {
			enemy = this->p2s.front();
		} else {
			enemy = this->p1s.front();
		}
		map<string, string> args;
		args["player"] = player;
		args["poke"] = poke;
		args["enemy"] = enemy;
		args["item"] = item;
		Event ev("mega", args);
		this->events.push_back(ev);
	}

	void faint(const string player, const string poke) {
		string killer;
		if (player == "p1") {
			killer = this->p2s.back();
			this->p1s.push_back("null");
		} else {
			killer = this->p1s.back();
			this->p2s.push_back("null");
		}
		map<string, string> args;
		args["player"] = player;
		args["poke"] = poke;
		args["killer"] = killer;
		Event ev("faint", args);
		this->events.push_back(ev);
	}

	void win(const string player) {
		map<string, string> args;
		args["player"] = player;
		Event ev("win", args);
	}

	void tie() {
		map<string, string> args;
		args["tie"] = "tie";
		Event ev("tie", args);
		this->events.push_back(ev);
	}

	vector<Event> getEvents() {
		return this->events;
	}
private:
	int num;
	vector<Event> events;
	vector<string> p1s;
	vector<string> p2s;
};

class Turns {
public:
	Turns(){
		Turn t(0);
		this->list.push_back(t);
	}

	void setTotal(const int t) {
		this->total = t;
	}

	Turn &curTurn() {
		return this->list.back();
	}

	void nextTurn(int num) {
		if (num > total) return; //Prevent Turn overflow
		Turn t(num);
		this->list.push_back(t);
	}

	vector<Turn> getTurns() {
		return this->list;
	}
private:
	int total;
	vector<Turn> list;
};

class Seed {
public:
	Seed() {}

	void setValues(const vector<unsigned int> rand) {
		this->arr = rand;
	}

	vector<unsigned int> getSeed() {
		return this->arr;
	}
private:
	vector<unsigned int> arr;
};

class Player {
public:
	Player(){

		this->name = "null";
		this->id = "null";
		this->rating = static_cast<float> (1000);
	}

	void setName(string name) {
		string id = toId(name);
		this->id = id;
		this->name = name;
		return;
	}

	void setTeam(Team t) {
		this->team = t;
		return;
	}

	vector<Mon> getTeam() {
		return this->team.getTeam();
	}

	void setRating(float rating = 1000) {
		this->rating = rating;
		return;
	}

	float getRating() {
		return this->rating;
	}

	string getId() {
		return this->id;
	}
private:
	string name;
	string id;
	Team team;
	float rating;
};

class Battle {
public:
	Player *p1 = new Player();
	Player *p2 = new Player();
	Turns *turns = new Turns();
	Battle(string json) {
		string w = json;
		this->calcSeed(w);
		this->turns->setTotal(this->calcTurns(w));

		vector<string> playerids = this->calcPlayerids(w);
		string p1name(playerids[0]);
		string p2name(playerids[1]);
		this->p1->setName(p1name);
		this->p2->setName(p2name);

		vector<Team> playerteams = this->calcTeams(w);
		this->p1->setTeam(playerteams[0]);
		this->p2->setTeam(playerteams[1]);

		this->parseLogs(w);
		this->finalize(w);
	}

	bool run() {
		return this->calculate;
	}

	float getRatingMod() {
		return this->ratingMod;
	}
private:
	Seed *seed = new Seed();
	vector<string> rules;
	bool calculate = true;
	float ratingMod = 1;

	string getPlayerById(const string id) {
		if (this->p1->getId() == id) {
			return "p1";
		} else {
			return "p2";
		}
	}

	void ignore() {
		cout << "Battle Flagged for Ignore" << endl;
		this->calculate = false;
	}

	void destroy() {
		delete this->seed;
		delete this->p1;
		delete this->p2;
		delete this->turns;
	}

	void calcSeed(string &json) {
		const string id = "{\"seed\":[";
		string::size_type f = json.find(id);

		if (f != string::npos) {
			//Found seed Identifier, get seed
			string::size_type end = json.find("]"); //Look for end of seed array
			if (end != string::npos) {
				string seedstr = json.substr(0, end);
				json.erase(0, end + sizeof(json.at(1)) * 2);
				seedstr.erase(0, 9);

				vector<unsigned int> values;
				vector<string> raw = split(seedstr, ",");

				for (unsigned int i = 0; i < sizeof(raw) / sizeof(raw[0]); i++) {
					values.push_back(stoul(raw[i]));
				}

				this->seed->setValues(values);
				return;
			}
		}

		//If this code is reached then we need to make a dummy seed
		vector<unsigned int> dummy = {0,0,0,0,0,0,0,0,0,0,0,0};
		this->seed->setValues(dummy);
	}

	int calcTurns(string &json) {
		const string id = "\"turns\":";
		string::size_type f = json.find(id);

		if (f != string::npos) {
			string::size_type end = json.find(",");

			if (end != string::npos) {
				string turnstr = json.substr(0, end);
				json.erase(0, end + sizeof(json.at(0)));
				turnstr.erase(0, 8);

				int turns = stoi(turnstr);
				return turns;
			}
		}

		return 0;
	}

	vector<string> calcPlayerids(string &json) {
		const string i0 = "\"p1\":";
		const string i1 = "\"p2\":";
		string::size_type f = json.find(i0);

		if (f != string::npos) {
			string::size_type f2 = json.find(i1);

			if (f2 != string::npos) {
				string::size_type splitter = json.find(",");

				if (splitter != string::npos) {
					string p1 = json.substr(0, splitter);
					p1.erase(0, 6);
					p1 = p1.substr(0, p1.find("\""));

					string p2 = json.substr(splitter);
					p2.erase(0, 7);
					p2 = p2.substr(0, p2.find("\""));

					json.erase(0, json.find("\"p1team\""));

					vector<string> ids;
					ids.push_back(p1);
					ids.push_back(p2);
					return ids;
				}
			}
		}

		vector<string> dummy;
		dummy.push_back("p1");
		dummy.push_back("p2");
		return dummy;
	}

	vector<Team> calcTeams(string &json) {
		const string i0 = "\"p1team\":[";
		const string i1 = "],\"p2team\":[";
		const string i2 = "],\"log\":";
		string::size_type f = json.find(i0);
		string p1 = "null";
		string p2 = "null";

		if (f != string::npos) {
			string::size_type f2 = json.find(i1);

			if (f2 != string::npos) {
				p1 = json.substr(0, f2);
				p1.erase(0, 10);

				string::size_type f3 = json.find(i2);

				if (f3 != string::npos) {
					p2 = json.substr(f2, f3);
					p2.erase(0, 12);
				}

				json.erase(0, f3 + sizeof(json.at(0)) * 2);
			}
		}

		vector<Team> res;

		if (p1 != "null" && p2 != "null") {
			vector<string> data;
			data.push_back(p1);
			data.push_back(p2);

			for (int i = 0; i < 2; i++) {
				vector<Mon> mons;
				for (int x = 0; x < 6; x++) {
					Mon poke;
					string id0;
					string id1;

					string::size_type ptr0;
					string::size_type ptr1;
					string::size_type ptr2;

					//Isolate Name
					string name;
					id0 = "{\"name\":\"";
					id1 = "\",";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						name = data[i].substr(0,ptr1);
						name.erase(0, 9);
						data[i].erase(0, ptr1 + sizeof(data[i].at(0)) * 2);
					}

					//Isolate Species
					string species;
					id0 = "\"species\":\"";
					id1 = "\",";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						species = data[i].substr(0,ptr1);
						species.erase(0, 11);
						data[i].erase(0, ptr1 + sizeof(data[i].at(0)) * 2);
					}

					//Isolate Moves
					vector<string> moves;
					string m;
					id0 = "\"moves\":[\"";
					id1 = "\"],";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						m = data[i].substr(0,ptr1);
						m.erase(0, 10);
						moves = split(m, "\",\"");
						data[i].erase(0, ptr1 + sizeof(data[i].at(0)) * 3);
					}

					//Isolate Ability
					string ability;
					id0 = "\"ability\":\"";
					id1 = "\",";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						ability = data[i].substr(0,ptr1);
						ability.erase(0, 11);
						data[i].erase(0, ptr1 + sizeof(data[i].at(0)) * 2);
					}

					//Isolate EVs
					map<string, int> evs;
					string n; //Key
					string o; //value
					id0 = "\"evs\":{\"";
					id1 = "},";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						m = data[i].substr(0,ptr1);
						m.erase(0, 8);

						//Isolate HP
						n = m.substr(0, 2);
						m.erase(0, 4);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						evs["hp"] = stoi(o);

						//Isolate Atk
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						evs["atk"] = stoi(o);

						//Isolate Def
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						evs["def"] = stoi(o);

						//Isolate Spa
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						evs["spa"] = stoi(o);

						//Isolate Spd
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						evs["spd"] = stoi(o);

						//Isolate Spe
						n = m.substr(0, 3);
						m.erase(0, 5);
						evs["spe"] = stoi(m);

						data[i].erase(0, ptr1 + sizeof(data[i].at(0)) * 2);
					}

					//Isolate IVs
					map<string, int> ivs;
					id0 = "\"ivs\":{\"";
					id1 = "},";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						m = data[i].substr(0,ptr1);
						m.erase(0, 8);

						//Isolate HP
						n = m.substr(0, 2);
						m.erase(0, 4);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						ivs["hp"] = stoi(o);

						//Isolate Atk
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						ivs["atk"] = stoi(o);

						//Isolate Def
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						ivs["def"] = stoi(o);

						//Isolate Spa
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						ivs["spa"] = stoi(o);

						//Isolate Spd
						n = m.substr(0, 3);
						m.erase(0, 5);
						ptr2 = m.find(",\"");
						o = m.substr(0, ptr2);
						m.erase(0, ptr2 + sizeof(m.at(0)) * 2);
						ivs["spd"] = stoi(o);

						//Isolate Spe
						n = m.substr(0, 3);
						m.erase(0, 5);
						ivs["spe"] = stoi(m);

						data[i].erase(0, ptr1 + sizeof(data[i].at(0)) * 2);
					}

					//Isolate Item
					string item;
					id0 = "\"item\":";
					id1 = ",";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						item = data[i].substr(0,ptr1);
						item.erase(0, 7);
						if (item.find("\"") == string::npos) {
							item = "null";
						} else {
							//Sanitize quotes
							item.erase(0, 1);
							item.pop_back();
						}
						data[i].erase(0, ptr1 + sizeof(data[i].at(0)));
					}

					//Isolate Level
					int level;
					id0 = "\"level\":";
					id1 = ",";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						m = data[i].substr(0,ptr1);
						m.erase(0, 8);
						level = stoi(m);
						data[i].erase(0, ptr1 + sizeof(data[i].at(0)));
					}

					//Isolate Shiny
					bool shiny;
					id0 = "\"shiny\":";
					id1 = ",";

					ptr0 = data[i].find(id0);
					ptr1 = data[i].find(id1);

					if (ptr0 != string::npos && ptr1 != string::npos) {
						m = data[i].substr(0,ptr1);
						m.erase(0, 8);
						if (m == "true") {
							shiny = true;
						} else {
							shiny = false;
						}
					}

					//Clean string to prepare for next poke
					ptr0 = data[i].find("},");
					data[i].erase(0, ptr0 + sizeof(data[i].at(0)) * 2);

					//Set Values
					poke.setName(name);
					poke.setSpecies(species);
					poke.setMoves(moves);
					poke.setAbility(ability);
					poke.setEvs(evs);
					poke.setIvs(ivs);
					poke.setItem(item);
					poke.setLevel(level);
					poke.setShiny(shiny);

					//Add Poke to Vector
					mons.push_back(poke);
				}

				Team t(mons);
				res.push_back(t);
			}
		}
		return res;
	}

	void parseLogs(string &json) {
		json.erase(0,9); //Sanitize Log head and Leading Quote
		string::size_type ptr0 = json.find("\"");
		string::size_type ptr1; //Spare Pointer
		string::size_type ptr2; //Spare Pointer
		string::size_type endline; //Endline Pointer
		string line; //Line chars
		string holder; //Spare String
		string holder2; //Spare String
		string token; //Switchcase String
		int turn = 0;

	parse: //Parse Label
		line = json.substr(0, ptr0);
		ptr1 = line.find("|");
		token = line.substr(0, ptr1);

		if (token == "player") {
			if (turn > 0) this->ignore();
		} else if (token == "rule") {
			holder = line.substr(ptr1 + sizeof(line.at(0)));
			this->rules.push_back(holder);
		} else if (token == "turn") {
			turn++;
			this->turns->nextTurn(turn);
		} else if (token == "switch") {
			holder = line.substr(ptr1 + sizeof(line.at(0)));
			this->turns->curTurn().switchin(holder.substr(0, 2), toId(holder.substr(5, holder.find("|"))));
		} else if (token == "move") {
			holder = line.substr(ptr1 + sizeof(line.at(0)));
			holder2 = holder.substr(holder.find("|") + sizeof(holder.at(0)));
			ptr2 = holder2.find("|");
			this->turns->curTurn().move(holder.substr(0, 2), toId(holder.substr(5, holder.find("|"))), toId(holder2.substr(0, ptr2)));
		} else if (token == "mega") {
			holder = line.substr(ptr1 + sizeof(line.at(0)));
			holder2 = holder.substr(holder.find("|") + sizeof(holder.at(0)));
			holder2 = holder2.substr(holder2.find("|") + sizeof(holder2.at(0)));
			this->turns->curTurn().mega(holder.substr(0, 2), toId(holder.substr(5, holder.find("|"))), toId(holder2));
		} else if (token == "faint") {
			holder = line.substr(ptr1 + sizeof(line.at(0)));
			holder2 = holder.substr(holder.find("|") + sizeof(holder.at(0)));
			holder2 = holder2.substr(holder2.find("|") + sizeof(holder2.at(0)));
			this->turns->curTurn().faint(holder.substr(0, 2), toId(holder.substr(5, holder.find("|"))));
		} else if (token == "win") {
			holder = this->getPlayerById(toId(line.substr(ptr1 + sizeof(line.at(0)))));
			this->turns->curTurn().win(holder);
			goto done;
		} else if (token == "tie") {
			this->turns->curTurn().tie();
			goto done;
		} else {
			//Bad Token

			/*
			case token == "teamsize":
			case token == "gametype":
			case token == "gen":
			case token == "tier":
			case token == "rated":
			case token == "seed":
			case token == "start":
			case token == "choice":
			case token == "upkeep":
			case token == "cant":
			case token == "detailschange":
			case token == "-start":
			case token == "-damage":
			case token == "-heal":
			case token == "-status":
			case token == "-boost":
			case token == "-unboost":
			case token == "-ability":
			case token == "-enditem":
			case token == "-supereffective":
			case token == "-resisted":
			case token == "-end":
			*/
		}

		//Clear line from string
		json.erase(0, ptr0 + sizeof(json.at(0)) * 4);

		//Check remaining JSON
		ptr0 = json.find("\"");
		goto parse;

	done:
		ptr0 = json.find("\"],");
		json.erase(0, ptr0 + sizeof(json.at(0)) * 3);
		return;
	}

	void finalize(string &json) {

		//Try to grab Ratings
		float p1 = 1000;
		float p2 = 1000;
		string holder;
		const string id0 = "\"p1rating\":";
		const string id1 = ",";

		const string::size_type ptr0 = json.find(id0);
		const string::size_type ptr1 = json.find(id1);

		if (ptr0 != string::npos && ptr1 != string::npos) {
			holder = json.substr(0,ptr1);
			holder.erase(0, 11);
			p1 = stof(holder);
			json.erase(0, ptr1 + sizeof(json.at(0)));

			//Now lets get P2 rating
			const string i0 = "\"p2rating\":";
			const string i1 = ",";

			const string::size_type pt0 = json.find(i0);
			const string::size_type pt1 = json.find(i1);
			if (pt0 != string::npos && pt1 != string::npos) {
				holder = json.substr(0,pt1);
				holder.erase(0, 11);
				p2 = stof(holder);
				json.erase(0, pt1 + sizeof(json.at(0)));

				this->calcRatingMod(p1, p2);
			} else {
				//Only one player rating is available?
				this->ignore();
			}

			this->p1->setRating(p1);
			this->p2->setRating(p2);
		}

		//Get End type - if not normal, we dont calc

		string endType;

		const string de = "\"endType\":\"";
		const string de2 = "\",";
		const string::size_type point0 = json.find(de);
		const string::size_type point1 = json.find(de2);

		endType = json.substr(point0 + 11,6);

		if (endType != "normal") this->ignore();
		json.erase();
	}

	void calcRatingMod(float p1, float p2) {
		if (abs(p1 - p2) > 300) this->ignore(); //Too far apart in rank
		float mod = 1;

		if (p1 > p2) {
			mod = p2 / p1; 
		} else {
			mod = p1 / p2;
		}

		this->ratingMod = mod;
	}
};

struct SMon {
	string id;
	vector<Prototype> protos;
	map<string, float> change;

	SMon(const string id, vector<Prototype> protos) {
		this->id = id;
		this->protos = protos;

		//Initialize default values
		this->change["sweep"] = static_cast<float> (0);
		this->change["sweepbreak"] = static_cast<float> (0);
		this->change["stall"] = static_cast<float> (0);
		this->change["stallbreak"] = static_cast<float> (0);
		this->change["support"] = static_cast<float> (0);
		this->change["momentum"] = static_cast<float> (0);
		this->change["offense"] = static_cast<float> (0);
		this->change["defense"] = static_cast<float> (0);
		this->change["special"] = static_cast<float> (0);
	}

	void update(const string id, float value) {
		this->change[id] += value;
	}

	void switchout(const string against, const string to, map<string, Pokemon*> &pokes) {
		if (against == "null" || to == "null") return;
		float e0 = pokes[against]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		Brain->receive(this, "switchout", d);
	}

	void switchin(const string against, const string from, map<string, Pokemon*> &pokes) {
		if (against == "null" || from == "null") return;
		float e0 = pokes[against]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		Brain->receive(this, "switchin", d);
	}

	void eswitch(const string from, const string to, map<string, Pokemon*> &pokes) {
		if (from == "null" || to == "null") return;
		float e0 = pokes[from]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		Brain->receive(this, "eswitch", d);
	}

	void move(const string target, string move, map<string, Pokemon*> &pokes) {
		if (target == "null" || move == "null") return;
		float e0 = pokes[target]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		string movestr = move;
		Brain->receive(this, movestr, d);
	}

	void emove(const string user, string move, map<string, Pokemon*> &pokes) {
		if (user == "null" || move == "null") return;
		float e0 = pokes[user]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		string movestr = "targeted--" + move;
		Brain->receive(this, movestr, d);
	}

	void mega(const string item, const string enemy, map<string, Pokemon*> &pokes) {
		if (item == "null" || enemy == "null") return;
		float e0 = pokes[enemy]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		Brain->receive(this, "mega", d);
	}

	void faint(const string killer, map<string, Pokemon*> &pokes) {
		if (killer == "null") return;
		float e0 = pokes[killer]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		Brain->receive(this, "faint", d);
	}

	void knockout(const string poke, map<string, Pokemon*> &pokes) {
		if (poke == "null") return;
		float e0 = pokes[poke]->e();
		float e1 = pokes[this->id]->e();
		float d = scale(e0,e1);
		Brain->receive(this, "knockout", d);
	}

	void win(map<string, Pokemon*> &pokes) {
		float e0 = pokes[this->id]->e();
		float d = scale(DEFAULTS.Pokes.EFF,e0);
		Brain->receive(this, "win", d);
	}

	void lose(map<string, Pokemon*> &pokes) {
		float e0 = pokes[this->id]->e();
		float d = scale(e0,DEFAULTS.Pokes.EFF);
		Brain->receive(this, "lose", d);
	}

	void tie(map<string, Pokemon*> &pokes) {
		float e0 = pokes[this->id]->e();
		float d = scale(e0,DEFAULTS.Pokes.EFF);
		Brain->receive(this, "tie", d);
	}

	map<string, float> exportMap(const float weight) {
		map<string, float> ex;
		map<string, float>::iterator it;
		for (it = this->change.begin(); it != this->change.end(); it++) {
			ex[it->first] = it->second * weight;
		}
		return ex;
	}
};

class Session {
public:
	Session(map<string, vector<Prototype>> &p1team, map<string, vector<Prototype>> &p2team, float weight) {
		this->p1team = p1team;
		this->p2team = p2team;
		this->weight = weight;

		map<string, vector<Prototype>>::iterator it;
		for (it = p1team.begin(); it != p1team.end(); it++) {
			SMon s(it->first, it->second);
			this->p1session.insert(pair<string, SMon>(it->first, s));;
		}

		for (it = p2team.begin(); it != p2team.end(); it++) {
			SMon s(it->first, it->second);
			this->p2session.insert(pair<string, SMon>(it->first, s));;
		}
	}

	map<string, vector<Prototype>> p1team;
	map<string, vector<Prototype>> p2team;
	map<string, SMon> p1session;
	map<string, SMon> p2session;

	void runEvent(Event e, map<string, Pokemon*> &pokes) {
		map<string, string> args = e.getArgs();
		string type = e.getType();

		if (type == "switch") {
			if (args["player"] == "p1") {
				if (args["from"] != "null") this->getP1Smon(args["from"]).switchout(args["enemy"], args["id"], pokes);
				if (args["id"] != "null") this->getP1Smon(args["id"]).switchin(args["enemy"], args["from"], pokes);
				if (args["enemy"] != "null") this->getP2Smon(args["enemy"]).eswitch(args["from"], args["id"], pokes);
			} else {
				if (args["from"] != "null") this->getP2Smon(args["from"]).switchout(args["enemy"], args["id"], pokes);
				if (args["id"] != "null") this->getP2Smon(args["id"]).switchin(args["enemy"], args["from"], pokes);
				if (args["enemy"] != "null") this->getP1Smon(args["enemy"]).eswitch(args["from"], args["id"], pokes);
			}
		} else if (type == "move") {
			if (args["player"] == "p1") {
				if (args["poke"] != "null") this->getP1Smon(args["poke"]).move(args["target"], args["move"], pokes);
				if (args["target"] != "null") this->getP2Smon(args["target"]).emove(args["poke"], args["move"], pokes);
			} else {
				if (args["poke"] != "null") this->getP2Smon(args["poke"]).move(args["target"], args["move"], pokes);
				if (args["target"] != "null") this->getP1Smon(args["target"]).emove(args["poke"], args["move"], pokes);
			}
		} else if (type == "mega") {
			if (args["player"] == "p1") {
				if (args["poke"] != "null") this->getP1Smon(args["poke"]).mega(args["item"], args["enemy"], pokes);
			} else {
				if (args["poke"] != "null") this->getP2Smon(args["poke"]).mega(args["item"], args["enemy"], pokes);
			}
		} else if (type == "faint") {
			if (args["player"] == "p1") {
				if (args["poke"] != "null") this->getP1Smon(args["poke"]).faint(args["killer"], pokes);
				if (args["killer"] != "null") this->getP2Smon(args["killer"]).knockout(args["poke"], pokes);
			} else {
				if (args["poke"] != "null") this->getP2Smon(args["poke"]).faint(args["killer"], pokes);
				if (args["killer"] != "null") this->getP1Smon(args["killer"]).knockout(args["poke"], pokes);
			}
		} else if (type == "win") {
			this->win(args["player"], pokes);
		} else if (type == "tie") {
			this->tie(pokes);
		} else {
			//Invalid
		}
	}

	void win(const string player, map<string, Pokemon*> &pokes) {
		map<string, SMon>::iterator it;
		if (player == "p1") {
			for (it = this->p1session.begin(); it != this->p1session.end(); it++) {
				it->second.win(pokes);
			}

			for (it = this->p2session.begin(); it != this->p2session.end(); it++) {
				it->second.lose(pokes);
			}
		} else {
			for (it = this->p1session.begin(); it != this->p1session.end(); it++) {
				it->second.lose(pokes);
			}

			for (it = this->p2session.begin(); it != this->p2session.end(); it++) {
				it->second.win(pokes);
			}
		}
	}

	void tie(map<string, Pokemon*> &pokes) {
		map<string, SMon>::iterator it;

		for (it = this->p1session.begin(); it != this->p1session.end(); it++) {
			it->second.tie(pokes);
		}

		for (it = this->p2session.begin(); it != this->p2session.end(); it++) {
			it->second.tie(pokes);
		}
	}

	vector<map<string, map<string, float>>> exportChanges() {
		vector<map<string, map<string, float>>> obj;

		map<string, map<string, float>> p1;
		map<string, SMon>::iterator it;
		for (it = this->p1session.begin(); it != this->p1session.end(); it++) {
			p1[it->first] = it->second.exportMap(this->weight);
		}

		obj.push_back(p1);

		map<string, map<string, float>> p2;
		for (it = this->p2session.begin(); it != this->p2session.end(); it++) {
			p1[it->first] = it->second.exportMap(this->weight);
		}

		obj.push_back(p2);

		return obj;
	}
private:
	float weight;

	SMon &getP1Smon(string id) {
		map<string, SMon>::iterator it;
		for (it = this->p1session.begin(); it != this->p1session.end(); it++) {
			if (it->first == id) return it->second;
		}
	}

	SMon &getP2Smon(string id) {
		map<string, SMon>::iterator it;
		for (it = this->p2session.begin(); it != this->p2session.end(); it++) {
			if (it->first == id) return it->second;
		}
	}
};

//Define these now that required classes have been declares

void K::apply(SMon *mon, float d) {
	float val = static_cast<float> (this->v * d);
	mon->update(this->c, val);
}

void Br::receive(SMon *mon, const string id, const float d) {
	if (!this->has(id)) {
		this->noHandler(id);
		return;
	}
	M m = this->get(id);

	map<string, K>::iterator it;
	for (it = m.k.begin(); it != m.k.end(); it++) {
		it->second.apply(mon, d);
	}

	//For now we're done. this may become more involved later
}

#endif