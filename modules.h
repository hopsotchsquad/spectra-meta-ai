
#ifndef MODULES_H
#define MODULES_H

class Loader : public Module {

private:
	string data;
public:
	Loader() {
		ifstream stream("data/data.bin");
		string buffer;
		string data;

		if (stream.is_open()) {
			while(getline(stream,buffer)) data += buffer;
			stream.close();
		}

		if (data.length() > 0) {
			this->status = success;
		} else {
			this->status = failed;
		}

		if (this->status == success) {
			cout << "Successfully loaded Pokemon Data..." << endl;
			this->data = data;
		} else if (this->status == failed) {
			cout << "No Pokemon Data Loaded, Building new Data Structure..." << endl;
			this->data = "nodata";
		} else {
			cerr << "Error in Data Loader" << endl;
		}
	}

	string getData() {
		if (this->data != "") {
			return this->data;
		} else {
			return "nodata";
		}
	}
};

class Mango : public Module {
public:
	Mango(){}

	Status parse(map<string, Pokemon*> &pokes, Battle data) {
		//Pokes is passed in by reference so we can modify it
		if (!data.run()) return success; //This battle is flagged to be ignored

		//Spawn Instances of both players pokes and grab nessecary protos
		vector<Mon> p1mons = data.p1->getTeam();
		vector<Mon> p2mons = data.p2->getTeam();

		cout << "Teams Loaded..." << endl;

		//Variables
		string species;
		vector<string> moves;
		string item;
		string abil;
		map<string, int> evs;
		map<string, int> ivs;
		vector<Prototype> aff;
		map<string, vector<Prototype>> p1team;
		map<string, vector<Prototype>> p2team;

		for (unsigned int i = 0; i < sizeof(p1mons) / sizeof(p1mons[0]); i++) {
			species = p1mons[i].getSpecies();
			moves = p1mons[i].getMoves();
			item = p1mons[i].getItem();
			abil = p1mons[i].getAbility();
			evs = p1mons[i].getEvs();
			ivs = p1mons[i].getIvs();

			aff = pokes[species]->instance(moves, item, abil, evs, ivs);
			p1team[species] = aff;
		}

		for (unsigned int i = 0; i < sizeof(p2mons) / sizeof(p2mons[0]); i++) {
			species = p2mons[i].getSpecies();
			moves = p2mons[i].getMoves();
			item = p2mons[i].getItem();
			abil = p2mons[i].getAbility();
			evs = p2mons[i].getEvs();
			ivs = p2mons[i].getIvs();

			aff = pokes[species]->instance(moves, item, abil, evs, ivs);
			p2team[species] = aff;
		}

		this->runBattle(pokes, data, p1team, p2team);
		return success;
	}
private:
	void runBattle(
		map<string, Pokemon*> &pokes,
		Battle battle,
		map<string, vector<Prototype>> &p1team,
		map<string, vector<Prototype>> &p2team
	) { //This is private to prevent being called outside of Mango.parse()
		cout << "RunBattle Fired..." << endl;
		float weight = battle.getRatingMod();

		//Session object is spawned to keep track of everything
		Session *s = new Session(p1team, p2team, weight);

		vector<Turn> t = battle.turns->getTurns();
		for (unsigned int i = 0; i < sizeof(t) / sizeof(t[0]); i++) {
			vector<Event> e = t[i].getEvents();
			for (unsigned int x = 0; x < sizeof(e) / sizeof(e[0]); x++) {
				s->runEvent(e[x], pokes);
			}
		}

		vector<map<string, map<string, float>>> change = s->exportChanges();

		this->logReport(this->genReport(battle, *s, change));

		this->commit(pokes, change, p1team, p2team);

		delete s;
	}

	ostringstream genReport(Battle battle, Session s, vector<map<string, map<string, float>>> change) {
		ostringstream iss;

		auto end = chrono::system_clock::now();
		time_t end_time = chrono::system_clock::to_time_t(end);

		string p1id = battle.p1->getId();
		float p1rating = battle.p1->getRating();
		string p2id = battle.p2->getId();
		float p2rating = battle.p2->getRating();

		iss << "Battle Report - " << p1id << " vs. " << p2id << "\n";
		iss << "Generated on " << end_time << "\n\n";

		iss << "P1->" << p1id << ":\n\n";
		iss << "ELO->" << p1rating << "\n";

		iss << "Team->{\n";

		map<string, vector<Prototype>>::iterator it;
		for (it = s.p1team.begin(); it != s.p1team.end(); it++) {
			iss << "Pokemon->" << it->first << "\n";
			iss << "Prototypes->{\n";

			for (unsigned int i = 0; i < sizeof(it->second) / sizeof(it->second[0]); i++) {
				iss << it->second[i].id << "->Ratings->{\n";
					map<string, Rating>::iterator it2;
					for (it2 = it->second[i].getRatings().begin(); it2 != it->second[i].getRatings().end(); it2++) {
						iss << it2->first << " - Initial(" << it2->second.getValue() << ") - Change: " << change[0][it->first][it2->first] << "\n";
					}
				iss << "}\n\n";
			}

			iss << "}\n\n";
		}
		iss << "}\n\n";

		iss << "P2->" << p2id << ":\n\n";
		iss << "ELO->" << p2rating << "\n";

		iss << "Team->{\n";

		for (it = s.p2team.begin(); it != s.p2team.end(); it++) {
			iss << "Pokemon->" << it->first << "\n";
			iss << "Prototypes->{\n";

			for (unsigned int i = 0; i < sizeof(it->second) / sizeof(it->second[0]); i++) {
				iss << it->second[i].id << "->Ratings->{\n";
					map<string, Rating>::iterator it2;
					for (it2 = it->second[i].getRatings().begin(); it2 != it->second[i].getRatings().end(); it2++) {
						iss << it2->first << " - Initial(" << it2->second.getValue() << ") - Change: " << change[1][it->first][it2->first] << "\n";
					}
				iss << "}\n\n";
			}

			iss << "}\n\n";
		}
		iss << "}\n";

		return iss;
	}

	void logReport(ostringstream report) {
		cout << "Log Report fired..." << endl;
		const chrono::system_clock::time_point now{std::chrono::system_clock::now()};
		const time_t now_{std::chrono::system_clock::to_time_t(now)};
		ostringstream filename;
		filename << "logs/" << std::put_time(localtime(&now_), "%Y-%m-%d %X") << ".log";

		ofstream stream(filename.str());

		if (stream.is_open()) {
			stream << report.str();
			stream.close();
		}
	}

	void commit(map<string, Pokemon*> &pokes, vector<map<string, map<string, float>>> change, map<string, vector<Prototype>> &p1team, map<string, vector<Prototype>> &p2team) {
		//Commit P1 changes
		map<string, map<string, float>>::iterator it;
		for (it = change[0].begin(); it != change[0].end(); it++) {
			for (unsigned int i = 0; i < sizeof(p1team[it->first]) / sizeof(p1team[it->first][0]); i++) {
				p1team[it->first][i].update(it->second);
			}
		}

		//Commit P2 changes
		for (it = change[1].begin(); it != change[1].end(); it++) {
			for (unsigned int i = 0; i < sizeof(p2team[it->first]) / sizeof(p2team[it->first][0]); i++) {
				p2team[it->first][i].update(it->second);
			}
		}

		map<string, vector<Prototype>>::iterator zz;
		for (zz = p1team.begin(); zz != p1team.end(); zz++) {
			pokes[zz->first]->update();
		}

		for (zz = p2team.begin(); zz != p2team.end(); zz++) {
			pokes[zz->first]->update();
		}
	}
};

#endif