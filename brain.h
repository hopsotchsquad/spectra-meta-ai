
//Brain

#ifndef BRAIN_H
#define BRAIN_H

//Brain Helper Structs

class SMon;

class Brain {
public:
	Brain() {
		this->m = this->loadBrain();
		this->w = this->loadWeights();
		this->a = this->loadAwaiting();
		this->s = this->loadStier();
	}

	void save() {
		this->writeBrain();
		this->writeWeights();
		this->writeAwaiting();
		this->writeStier();
	}

	map<string, float> getWeights() {
		return this->w;
	}

	vector<string> getAwaiting() {
		return this->a;
	}

	vector<string> getStier() {
		return this->s;
	}

	void receive(SMon *mon, const string id, const float d) {
		if (!this->has(id)) {
			this->noHandler(id);
			return;
		}
		M m = this->get(id);

		map<string, K>::iterator it;
		for (it = m.k.begin(); it != m.k.end(); it++) {
			it->second.apply(mon, d);
		}

		//For now we're done. this may become more involved later
	}

	float calcEff(vector<map<string, Rating>> protos) {
		vector<float> eff;
		unsigned int i;
		for (i = 0; i < sizeof(protos) / sizeof(protos[0]); i++) {
			map<string, float> weighted = this->applyWeights(protos[i]);
			eff.push_back(this->combine(weighted));
		}

		float final = 0;

		for (i = 0; i < sizeof(eff) / sizeof(eff[0]); i++) {
			final += eff[i];
		}

		final = static_cast<float> (final / i);
		return final;
	}
private:
	map<string, M> m; //Brain Data
	map<string, float> w; //Weights
	vector<string> a; //Awaiting
	vector<string> s; //S tier

	float combine(map<string, float> ratings) {
		float out = 0;
		float div = 9;
		map<string, float>::iterator it;
		for (it = ratings.begin(); it != ratings.end(); it++) {
			out += it->second;
		}
		out = static_cast<float> (out / div);
		return out;
	}

	map<string, float> applyWeights(map<string, Rating> proto) {
		map<string, float> out;
		map<string, Rating>::iterator it;
		for (it = proto.begin(); it != proto.end(); it++) {
			out[it->first] = it->second.getValue() * this->w[it->first];
		}
		return out;
	}

	void noHandler(const string id) {
		bool exists = false;
		for (unsigned int i = 0; sizeof(this->a) / sizeof(this->a[0]); i++) {
			if (this->a[i] == id) exists = true;
		}

		if (exists) return; //Already awaiting

		this->a.push_back(id);
		this->writeAwaiting();
		cout << "awaiting," << id << endl;
	}

	map<string, M> loadBrain() {
		ifstream stream("data/brain.csv");
		string buffer;
		map<string, M> br;

		if (stream.is_open()) {
			while(getline(stream,buffer)) this->unpack(buffer, br);
			stream.close();
		}

		return br;
	}

	map<string, float> loadWeights() {
		ifstream stream("data/weights.csv");
		string buffer;
		map<string, float> out;

		if (stream.is_open()) {
			while(getline(stream, buffer)) {
				string::size_type ptr0 = buffer.find("=");
				out[buffer.substr(0, ptr0)] = stof(buffer.substr(ptr0 + sizeof(buffer.at(0)), buffer.find("\n")));
			}
			stream.close();
		}

		return out;
	}

	vector<string> loadAwaiting() {
		ifstream stream("data/awaiting.csv");
		string buffer;
		vector<string> out;

		if (stream.is_open()) {
			while(getline(stream, buffer)) out.push_back(buffer);
			stream.close();
		}

		return out;
	}

	vector<string> loadStier() {
		ifstream stream("data/stier.csv");
		string buffer;
		vector<string> out;

		if (stream.is_open()) {
			while(getline(stream, buffer)) out.push_back(buffer);
			stream.close();
		}

		return out;
	}

	void writeBrain() {
		ostringstream data = this->pack();
		ofstream stream("data/brain.csv");
		if (stream.is_open()) {
			stream << data.str();
			stream.close();
		}
	}

	void writeWeights() {
		ofstream stream("data/weights.csv");
		if (stream.is_open()) {
			map<string, float>::iterator it;
			for (it = this->w.begin(); it != this->w.end(); it++) {
				stream << it->first << "=" << it->second << "\n";
			}
			stream.close();
		}
	}

	void writeAwaiting() {
		ofstream stream("data/awaiting.csv");
		if (stream.is_open()) {
			for (unsigned int i = 0; sizeof(this->a) / sizeof(this->a[0]); i++) {
				stream << this->a[i] << "\n";
			}
			stream.close();
		}
	}

	void writeStier() {
		ofstream stream("data/stier.csv");
		if (stream.is_open()) {
			for (unsigned int i = 0; sizeof(this->a) / sizeof(this->a[0]); i++) {
				stream << this->a[i] << "\n";
			}
			stream.close();
		}
	}

	bool has(const string id) {
		map<string, M>::iterator i = this->m.find(id);
		if (i != this->m.end()) {
			return true;
		} else {
			return false;
		}
	}

	M get(const string id) {
		//Never Call without calling Has beforehand!
		return this->m[id];
	}

	void unpack(string buffer, map<string, M> &br) {
		const string d = "|";

		string id = "null";
		string::size_type ptr = buffer.find(d);

		if (ptr != string::npos) {
			id = buffer.substr(0, ptr);
			buffer.erase(0, ptr + sizeof(buffer.at(0)));
		}

		if (id != "null") {
			vector<string> Ks = split(buffer, ",");
			map<string, K> built;

			for (unsigned int i = 0; i < sizeof(Ks) / sizeof(Ks[0]); i++) {
				vector<string> parts = split(Ks[i], "=");
				K k(parts[0], stof(parts[1]));
				built[parts[0]] = k;
			}

			M m(id, built);

			br[id] = m;
		}
	}

	ostringstream pack() {
		ostringstream ss;

		map<string, M>::iterator it;
		for (it = this->m.begin(); it != this->m.end(); it++) {
			ss << it->second.stringify() << "\n";
		}

		return ss;
	}
};

#endif