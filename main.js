'use strict';
const {spawn} = require('child_process');
const chalk = require('chalk');
const clear = require('clear');
const figlet = require('figlet');
class M extends require("events") {
	constructor() {
		super();
		this.process = spawn("./main",[1],{shell:true});
		this.process.stdout.on("data",(data) => {
			const parts = data.toString().trim().split(",");
			switch(parts[0]) {
			case "awaiting": this.awaiting(parts[1]); break;
			case "awaitlist": this.emit("awaitlist", parts[1]); break;
			case "tiersregen": console.log(chalk.bold("Tiers Regenerated")); break;
			case "battleparsed": this.emit("parsed"); break;
			case "ready": prompt(); break;
			default: console.log(parts.join(","));
			}
		});
		this.process.stderr.on("data", (data) => {
			console.log(chalk.red.bold("Error from SubProcess"));
			console.log(data.toString());
			prompt();
		});
		this.process.on("close", (code) => {
			console.log(chalk.red.bold("SubProcess Closed."));
			console.log("Exit Code: " + code);
			prompt();
		});
	}
	send(data) {
		if (!this.process || !this.process.stdin) return console.log("Error sending data to SubProcess");
		this.process.stdin.write("" + data);
		this.process.stdin.end();
	}
	awaiting(id) {console.log("Awaiting " + id);}
	getAwait(callback) {
		this.once("awaitlist", (data) => {return callback(data);});
		this.process.send("getawait");
	}
};
global.cLib = require("./lib/command_library.js");
global.Main = {};
global.init = function() {Main = new M();}
global.prompt = function() { process.stdout.write("[Spectra-Meta Test Wrapper]$ "); process.stdin.resume(); };
figlet("Spectra Meta Test Wrapper", function(err, data) {
	console.log(chalk.blue.bold(data));
	console.log(chalk.red.bold("This is a Node.js Wrapper for the Spectra-Meta Process."));
	console.log(chalk.bold("This is intended for debug purposes only - do not use this in a production environment.\nYou can use the command `help` for a list of commands.\n"));
	prompt();
	process.stdin.setEncoding('utf8');
	process.stdin.on("data", async function(input) {
		const args = input.split(",");
		const cmd = args.shift().trim();
		const operation = cLib.fetch(cmd);
		if (!operation) return cLib.noOp(cmd);
		const err = await operation.run(this, args);
		if (err) {
			console.log(chalk.red.bold("FATAL ERROR - EXITING..."));
			process.exit();
		} else if (cmd !== "start" && cmd !== "reload") prompt();
	}.bind(this));
}.bind(this));