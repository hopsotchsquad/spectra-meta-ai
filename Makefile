OBJS = main.cpp
CC = g++
COMPILER_FLAGS = -std=gnu++14 -w
OBJ_NAME = main

all : $(OBJS)
	$(CC) $(OBJS) $(COMPILER_FLAGS) -o $(OBJ_NAME)