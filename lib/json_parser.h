
//JSON Parser

#ifndef JSON_PARSER_H
#define JSON_PARSER_H

class Battle;

namespace JSON {
	Battle parse(const string JSON) {
		Battle b(JSON);
		return b;
	}
}

#endif